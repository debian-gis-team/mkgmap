#! /bin/sh
# Simple wrapper script to execute mkgmap in Debian

set -e

JAVA_CMDS="${JAVA_HOME}/bin/java"
JAVA_CMDS="${JAVA_CMDS} /usr/bin/java"

if [ -z "${JAVACMD}" ]; then
    for jcmd in $JAVA_CMDS; do
        if [ -x "$jcmd" ] && [ -z "${JAVACMD}" ]; then
            JAVACMD="$jcmd"
            break
        fi
    done
fi

if [ "$JAVACMD" ]; then
    echo "Using $JAVACMD to execute mkgmap."
    exec "${JAVACMD}" -jar /usr/share/mkgmap/mkgmap.jar "$@"
else
    echo "No valid JVM found to run mkgmap."
    exit 1
fi
