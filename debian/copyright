Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Steve Ratcliffe <sr@parabola.me.uk>
Upstream-Name: mkgmap
Source: https://www.mkgmap.org.uk/download/

Files: *
Copyright: 2006-2015, Steve Ratcliffe <sr@parabola.me.uk>
                2009, Bernhard Heibler
                2009, Christian Gawron
                2009, Clinton Gladstone
                2009, Toby Speight
                2010, Jeffrey C. Ollie
                2015, Maxim Duester
License: GPL-2+

Files: resources/typ-files/mapnik.txt
 src/uk/me/parabola/mkgmap/combiners/GmapiBuilder.java
 src/uk/me/parabola/imgfmt/app/dem/DEMFile.java
 src/uk/me/parabola/imgfmt/app/dem/DEMHeader.java
 src/uk/me/parabola/imgfmt/app/dem/DEMSection.java
 src/uk/me/parabola/imgfmt/app/dem/DEMTile.java
 src/uk/me/parabola/imgfmt/app/mdr/LargeListSorter.java
 src/uk/me/parabola/imgfmt/app/net/Abandon.java
 src/uk/me/parabola/imgfmt/app/net/AngleChecker.java
 src/uk/me/parabola/imgfmt/app/net/VarBitWriter.java
 src/uk/me/parabola/imgfmt/app/srt/DoubleSortKey.java
 src/uk/me/parabola/imgfmt/app/srt/SrtFileReader.java
 src/uk/me/parabola/imgfmt/mps/MpsBlock.java
 src/uk/me/parabola/imgfmt/Sized.java
 src/uk/me/parabola/imgfmt/sys/FileLink.java
 src/uk/me/parabola/io/FileBlock.java
 src/uk/me/parabola/mkgmap/combiners/SubFileInfo.java
 src/uk/me/parabola/mkgmap/filters/MustSplitException.java
 src/uk/me/parabola/mkgmap/filters/PredictFilterPoints.java
 src/uk/me/parabola/mkgmap/general/CityInfo.java
 src/uk/me/parabola/mkgmap/general/ZipCodeInfo.java
 src/uk/me/parabola/mkgmap/osmstyle/eval/NotRegexOp.java
 src/uk/me/parabola/mkgmap/osmstyle/ExpressionArranger.java
 src/uk/me/parabola/mkgmap/osmstyle/housenumber/ExtNumbers.java
 src/uk/me/parabola/mkgmap/osmstyle/housenumber/HousenumberGroup.java
 src/uk/me/parabola/mkgmap/osmstyle/housenumber/HousenumberIvl.java
 src/uk/me/parabola/mkgmap/osmstyle/housenumber/HousenumberRoad.java
 src/uk/me/parabola/mkgmap/osmstyle/NameFinder.java
 src/uk/me/parabola/mkgmap/osmstyle/NearbyPoiHandler.java
 src/uk/me/parabola/mkgmap/osmstyle/optional/DebugWriter.java
 src/uk/me/parabola/mkgmap/osmstyle/PrefixSuffixFilter.java
 src/uk/me/parabola/mkgmap/reader/hgt/HGTConverter.java
 src/uk/me/parabola/mkgmap/reader/hgt/HGTList.java
 src/uk/me/parabola/mkgmap/reader/hgt/HGTReader.java
 src/uk/me/parabola/mkgmap/reader/osm/HousenumberHooks.java
 src/uk/me/parabola/mkgmap/reader/osm/MultiPolygonCutter.java
 src/uk/me/parabola/mkgmap/reader/osm/ResidentialHook.java
 src/uk/me/parabola/util/KdTree.java
 src/uk/me/parabola/util/Locatable.java
 src/uk/me/parabola/util/ShapeSplitter.java
 test/main/RulesTest.java
 test/uk/me/parabola/imgfmt/app/dem/DemTileTest.java
 test/uk/me/parabola/imgfmt/app/net/NumbersTest.java
 test/uk/me/parabola/mkgmap/filters/LineSplitterFilterTest.java
 test/uk/me/parabola/mkgmap/osmstyle/ExpressionArrangerTest.java
 test/uk/me/parabola/mkgmap/osmstyle/PrefixSuffixFilterTest.java
 test/uk/me/parabola/mkgmap/reader/hgt/HGTConverterTest.java
 test/uk/me/parabola/util/KdTreeTest.java
 test/uk/me/parabola/util/ShapeSplitterTest.java
 tools/buildoptions/OptionsBuilder.java
Copyright: 2014-2015, Gerd Petermann
                2018, Steve Ratcliffe
                2020, Mike Baggaley
License: GPL-2 or GPL-3

Files: scripts/gmapi-builder.py
Copyright: 2009, Berteun Damman
License: BSD-3-Clause

Files: debian/*
Copyright: 2008, Andreas Putzo <andreas@putzo.net>
           2009, Francesco Paolo Lovergine <frankie@debian.org>
           2014, Andreas Tille <tille@debian.org>
License: GPL-2+

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 or
 version 2 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-2+
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 2 as
    published by the Free Software Foundation.
 .
    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
 .
    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 3 as
 published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the OpenStreetMap Project nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY
 EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
